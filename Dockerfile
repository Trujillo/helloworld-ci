FROM ubuntu:latest
RUN apt-get -y update
RUN apt-get -y install apache2
RUN apt-get install maven -y
RUN apt-get update
RUN apt update
RUN apt-get install curl -y
RUN apt-get install nodejs npm -y
EXPOSE 80
CMD apachectl -D FOREGROUND
